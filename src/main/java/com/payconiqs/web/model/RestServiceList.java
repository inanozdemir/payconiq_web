package com.payconiqs.web.model;


/**
 * Created by inano on 7.01.2017.
 */

public class RestServiceList {

    private String baseUrl;
    private String listAllStocks;
    private String statistics;

    private String serverTokenUrl;
    private String serverOAuthClientId;
    private String serverOAuthClientSecret;
    private String serverOAuthId;
    private String[] serverOAuthClientScope;

    public String getBaseUrl() {
        return baseUrl;
    }

    public void setBaseUrl(String baseUrl) {
        this.baseUrl = baseUrl;
    }

    public String getListAllStocks() {
        return listAllStocks;
    }

    public void setListAllStocks(String listAllStocks) {
        this.listAllStocks = listAllStocks;
    }

    public String getStatistics() {
        return statistics;
    }

    public void setStatistics(String statistics) {
        this.statistics = statistics;
    }

    public String getServerTokenUrl() {
        return serverTokenUrl;
    }

    public void setServerTokenUrl(String serverTokenUrl) {
        this.serverTokenUrl = serverTokenUrl;
    }

    public String getServerOAuthClientId() {
        return serverOAuthClientId;
    }

    public void setServerOAuthClientId(String serverOAuthClientId) {
        this.serverOAuthClientId = serverOAuthClientId;
    }

    public String getServerOAuthClientSecret() {
        return serverOAuthClientSecret;
    }

    public void setServerOAuthClientSecret(String serverOAuthClientSecret) {
        this.serverOAuthClientSecret = serverOAuthClientSecret;
    }

    public String getServerOAuthId() {
        return serverOAuthId;
    }

    public void setServerOAuthId(String serverOAuthId) {
        this.serverOAuthId = serverOAuthId;
    }

    public String[] getServerOAuthClientScope() {
        return serverOAuthClientScope;
    }

    public void setServerOAuthClientScope(String[] serverOAuthClientScope) {
        this.serverOAuthClientScope = serverOAuthClientScope;
    }
}
