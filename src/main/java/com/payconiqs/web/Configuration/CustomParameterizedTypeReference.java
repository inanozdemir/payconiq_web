package com.payconiqs.web.Configuration;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import com.payconiqs.web.model.Stock;
import com.payconiqs.web.service.ServerMetrics;

import java.util.Collection;

/**
 * Created by inano on 13.03.2018.
 */

@Component("customParameterizedTypeReference")
public class CustomParameterizedTypeReference {

    public <T> ParameterizedTypeReference<Collection<Stock>> parameterizedTypeCollectionOfStocks(){
        ParameterizedTypeReference<Collection<Stock>> parameterizedTypeReference =
                new ParameterizedTypeReference<Collection<Stock>>(){};
        return parameterizedTypeReference;
    }


    public <T> ParameterizedTypeReference<ServerMetrics> parameterizedTypeServerMetrics(){
        ParameterizedTypeReference<ServerMetrics> parameterizedTypeReference =
                new ParameterizedTypeReference<ServerMetrics>(){};
        return parameterizedTypeReference;
    }
}

