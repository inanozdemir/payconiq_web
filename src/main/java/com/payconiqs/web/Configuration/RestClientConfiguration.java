package com.payconiqs.web.Configuration;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.payconiqs.web.model.RestServiceList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.Scope;
import org.springframework.core.env.Environment;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

/**
 * Created by inano on 13.03.2018.
 */
@Configuration
@ComponentScan
@PropertySource({ "classpath:application.properties" })
@EnableWebMvc
public class RestClientConfiguration {

    @Autowired
    private Environment env;

    @Bean
    public RestServiceList mockServerProperties(){
        RestServiceList mockServerProperties = new RestServiceList();
        mockServerProperties.setBaseUrl(env.getProperty("restserver.baseurl"));
        mockServerProperties.setListAllStocks(env.getProperty("restserver.listall"));
        mockServerProperties.setStatistics(env.getProperty("restserver.statistics"));

        mockServerProperties.setServerTokenUrl(env.getProperty("mockserver.tokenUrl"));
        mockServerProperties.setServerOAuthClientId(env.getProperty("mockserver.oauth.client.id"));
        mockServerProperties.setServerOAuthId(env.getProperty("mockserver.oauth.id"));
        mockServerProperties.setServerOAuthClientSecret(env.getProperty("mockserver.oauth.client.secret"));
        mockServerProperties.setServerOAuthClientScope(env.getProperty("mockserver.oauth.client.scope",String[].class));


        return mockServerProperties;
    }

    @Bean
    @Scope("prototype")
    public ObjectMapper objectMapper(){
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        mapper.registerModule(new SimpleModule());
        return  mapper;
    }

    @Bean
    @Scope("prototype")
    public MappingJackson2HttpMessageConverter mappingJackson2HttpMessageConverter(){
        MappingJackson2HttpMessageConverter converter = new MappingJackson2HttpMessageConverter();
        converter.setSupportedMediaTypes(MediaType.parseMediaTypes("application/hal+json"));
        converter.setObjectMapper(objectMapper());
        return converter;
    }

    @Bean
    @Scope("prototype")
    public RestTemplateGenerator restTemplateBuilder(){
        return new RestTemplateGenerator();
    }

}
