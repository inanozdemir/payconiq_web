package com.payconiqs.web.Configuration;

/**
 * Created by inano on 8.01.2017.
 */

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.stereotype.Component;

@Component("customHttpHeaders")
public class CustomHttpHeaders {

    public HttpEntity<HttpHeaders> customHttpHeadersHttpEntity(OAuth2AccessToken accessToken){
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Bearer  " + accessToken.getValue());
        headers.add("Content-Type", "application/hal+json");
        HttpEntity<HttpHeaders> httpHeadersHttpEntity = new HttpEntity<>(headers);
        return httpHeadersHttpEntity;
    }
}
