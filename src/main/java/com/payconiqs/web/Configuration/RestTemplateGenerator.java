package com.payconiqs.web.Configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.util.Collections;

/**
 * Created by inano on 13.03.2018.
 */
public class RestTemplateGenerator {
    @Autowired
    private MappingJackson2HttpMessageConverter mappingJackson2HttpMessageConverter;

    public RestTemplate restTemplateWithJackson2Convertor(){
        RestTemplate restTemplate =
                new RestTemplate(Collections.singletonList(mappingJackson2HttpMessageConverter));
        return restTemplate;
    }


}
