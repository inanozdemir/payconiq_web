package com.payconiqs.web.service;

import com.payconiqs.web.model.RestServiceList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.oauth2.client.DefaultOAuth2ClientContext;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.security.oauth2.client.token.grant.client.ClientCredentialsResourceDetails;
import org.springframework.security.oauth2.common.OAuth2AccessToken;

import java.util.Arrays;

/**
 * Created by inano on 8.01.2017.
 */
public abstract class AbstractRestApiService {

    @Autowired
    private RestServiceList restServiceList;

    public AbstractRestApiService(){

    }

    protected OAuth2AccessToken getOAuthToken() {
        ClientCredentialsResourceDetails resource = new ClientCredentialsResourceDetails();
        resource.setAccessTokenUri("http://localhost:8080/oauth/token");
        resource.setClientId("rest-api-client");
        resource.setId("stocks-api");
        resource.setClientSecret("psw");
        resource.setScope(Arrays.asList("read", "write", "trust"));

        DefaultOAuth2ClientContext clientContext = new DefaultOAuth2ClientContext();
        OAuth2RestTemplate restTemplate = new OAuth2RestTemplate(resource, clientContext);

        return restTemplate.getAccessToken();
    }

    protected OAuth2RestTemplate oAuth2RestTemplate() {
        OAuth2RestTemplate restTemplate = new OAuthAbstractSecurityConfigurationBuilder()
                .setTokenUrl(restServiceList.getServerTokenUrl())
                .setClientId(restServiceList.getServerOAuthClientId())
                .setClientSecret(restServiceList.getServerOAuthClientSecret())
                .setId(restServiceList.getServerOAuthId())
                .setScope(restServiceList.getServerOAuthClientScope())
                .build();
        return restTemplate;
    }

    public RestServiceList getRestServiceList() {
        return restServiceList;
    }

    protected class OAuthAbstractSecurityConfigurationBuilder{
        private String tokenUrl;
        private String clientId;
        private String id;
        private String clientSecret;
        private String[] scope;

        public String getTokenUrl() {
            return tokenUrl;
        }

        public OAuthAbstractSecurityConfigurationBuilder setTokenUrl(String tokenUrl) {
            this.tokenUrl = tokenUrl;
            return this;
        }

        public String getClientId() {
            return clientId;
        }

        public OAuthAbstractSecurityConfigurationBuilder setClientId(String clientId) {
            this.clientId = clientId;
            return this;
        }

        public String getId() {
            return id;
        }

        public OAuthAbstractSecurityConfigurationBuilder setId(String id) {
            this.id = id;
            return this;
        }

        public String getClientSecret() {
            return clientSecret;
        }

        public OAuthAbstractSecurityConfigurationBuilder setClientSecret(String clientSecret) {
            this.clientSecret = clientSecret;
            return this;
        }

        public String[] getScope() {
            return scope;
        }

        public OAuthAbstractSecurityConfigurationBuilder setScope(String[] scope) {
            this.scope = scope;
            return this;
        }


        public OAuth2RestTemplate build(){

            return getInstance(this);
        }

        private OAuth2RestTemplate getInstance(OAuthAbstractSecurityConfigurationBuilder builder){
            ClientCredentialsResourceDetails resource = new ClientCredentialsResourceDetails();
            resource.setAccessTokenUri(builder.tokenUrl);
            resource.setClientId(builder.clientId);
            resource.setId(builder.id);
            resource.setClientSecret(builder.clientSecret);
            resource.setScope(Arrays.asList(builder.scope));

            OAuth2RestTemplate restTemplate = new OAuth2RestTemplate(resource, new DefaultOAuth2ClientContext());

            return restTemplate;
        }
    }
}

