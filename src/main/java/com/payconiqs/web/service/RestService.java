package com.payconiqs.web.service;

import com.payconiqs.web.Configuration.CustomHttpHeaders;
import com.payconiqs.web.Configuration.CustomParameterizedTypeReference;
import com.payconiqs.web.Configuration.RestTemplateGenerator;
import com.payconiqs.web.model.Stock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.text.MessageFormat;
import java.util.Collection;
import java.util.Collections;

/**
 * Created by inano on 13.03.2018.
 */
@Service
public class RestService extends AbstractRestApiService{

    @Autowired
    private RestTemplateGenerator restTemplateGenerator;

    @Autowired
    @Qualifier(value = "customParameterizedTypeReference")
    private CustomParameterizedTypeReference customParameterizedTypeReference;

    @Autowired
    @Qualifier(value = "customHttpHeaders")
    private CustomHttpHeaders customHttpHeaders;

    public RestService(){

    }

    public Collection<Stock> getAllStocks() {
        try {
            MessageFormat messageFormat = new MessageFormat(getRestServiceList().getListAllStocks());
            String url = messageFormat.toPattern();

            HttpEntity<HttpHeaders> header = customHttpHeaders.customHttpHeadersHttpEntity(getOAuthToken());

            OAuth2RestTemplate oAuth2RestTemplate = oAuth2RestTemplate();
            ResponseEntity<Collection<Stock>> list = oAuth2RestTemplate.exchange(
                    url,
                    HttpMethod.GET,
                    header,
                    customParameterizedTypeReference.parameterizedTypeCollectionOfStocks());

            return list.getBody();
        } catch (RestClientException e) {
            e.printStackTrace();
        }
        return null;
    }

    public ServerMetrics getServerMetric() {
        MessageFormat messageFormat = new MessageFormat(getRestServiceList().getStatistics());
        String url = messageFormat.format(new Object[]{});

        ResponseEntity<ServerMetrics> list = restTemplateGenerator.restTemplateWithJackson2Convertor().exchange(
                url,
                HttpMethod.GET,
                customHttpHeaders.customHttpHeadersHttpEntity(getOAuthToken()),
                customParameterizedTypeReference.parameterizedTypeServerMetrics());

        return list.getBody();
    }
}
