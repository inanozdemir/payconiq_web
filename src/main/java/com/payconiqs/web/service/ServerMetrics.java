package com.payconiqs.web.service;

/**
 * Created by inano on 9.01.2017.
 */
public class ServerMetrics {
    String status2xx;
    String status4xx;
    String status5xx;

    String maxTimOfAllRequests;
    String avgTimeOfAllRequests;
    String numberOfTotalRequests;
    String minTimOfAllRequests;

    public String getMaxTimOfAllRequests() {
        return maxTimOfAllRequests;
    }

    public void setMaxTimOfAllRequests(String maxTimOfAllRequests) {
        this.maxTimOfAllRequests = maxTimOfAllRequests;
    }

    public String getAvgTimeOfAllRequests() {
        return avgTimeOfAllRequests;
    }

    public void setAvgTimeOfAllRequests(String avgTimeOfAllRequests) {
        this.avgTimeOfAllRequests = avgTimeOfAllRequests;
    }

    public String getNumberOfTotalRequests() {
        return numberOfTotalRequests;
    }

    public void setNumberOfTotalRequests(String numberOfTotalRequests) {
        this.numberOfTotalRequests = numberOfTotalRequests;
    }

    public String getMinTimOfAllRequests() {
        return minTimOfAllRequests;
    }

    public void setMinTimOfAllRequests(String minTimOfAllRequests) {
        this.minTimOfAllRequests = minTimOfAllRequests;
    }

    public String getStatus2xx() {
        return status2xx;
    }

    public void setStatus2xx(String status2xx) {
        this.status2xx = status2xx;
    }

    public String getStatus4xx() {
        return status4xx;
    }

    public void setStatus4xx(String status4xx) {
        this.status4xx = status4xx;
    }

    public String getStatus5xx() {
        return status5xx;
    }

    public void setStatus5xx(String status5xx) {
        this.status5xx = status5xx;
    }
}
