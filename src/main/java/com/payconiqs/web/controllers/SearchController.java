package com.payconiqs.web.controllers;

import com.payconiqs.web.model.Stock;
import com.payconiqs.web.service.RestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.Collection;

/**
 * Created by inano on 13.03.2018.
 */
@Component
@Scope("request")
public class SearchController {

    @Autowired
    RestService restService;

    private Collection<Stock> stockList;

    @PostConstruct
    public void init() {
        this.stockList = restService.getAllStocks();
    }

    public Collection<Stock> getStockList() {
        return stockList;
    }

    public void setStockList(Collection<Stock> stockList) {
        this.stockList = stockList;
    }
}
