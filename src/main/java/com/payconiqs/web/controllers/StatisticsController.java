package com.payconiqs.web.controllers;

import com.payconiqs.web.service.RestService;
import com.payconiqs.web.service.ServerMetrics;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * Created by inano on 13.03.2018.
 */
@Component
@Scope("request")
public class StatisticsController {

    @Autowired
    RestService restService;

    private ServerMetrics serverMetrics;

    public StatisticsController(){

    }

    @PostConstruct
    public void init() {
        serverMetrics = restService.getServerMetric();
    }

    public RestService getRestService() {
        return restService;
    }

    public void setRestService(RestService restService) {
        this.restService = restService;
    }

    public ServerMetrics getServerMetrics() {
        return serverMetrics;
    }

    public void setServerMetrics(ServerMetrics serverMetrics) {
        this.serverMetrics = serverMetrics;
    }
}
