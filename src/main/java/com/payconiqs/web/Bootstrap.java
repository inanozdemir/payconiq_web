package com.payconiqs.web;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Profile;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.support.PropertiesLoaderUtils;
import org.springframework.web.WebApplicationInitializer;

import javax.servlet.ServletContext;
import java.io.IOException;
import java.util.Properties;

/**
 * Bootstrap for the entire application
 * 
 * Created by inano on 5.01.2017.
 *
 */

public class Bootstrap implements WebApplicationInitializer {


	private static final Logger LOGGER = LogManager.getLogger();

	@Override
	public void onStartup(ServletContext container) {
		try {

		} catch (Exception e) {
			LOGGER.catching(e);
		}
	}

	@Profile("APPLICATION")
	static class ApplicationProperties {
		private static Properties properties;

		@Bean
		public static Properties getProperties() throws IOException {
			if (properties == null) {
				properties = PropertiesLoaderUtils.loadProperties(new ClassPathResource("application.properties"));
			}

			return properties;
		}
	}
}