$(document).ready(function () {
    $("#" + location.href.split("pages/")[1].split('.html')[0])
        .addClass("active");

    $('.popper').popover({
        placement: 'right',
        container: 'body',
        trigger: 'hover',
        html: true,
        content: function () {
            return $(this).next('.media').html();
        }
    });

    $('.help').popover({
        container: 'body',
        trigger: 'hover'
    });
});